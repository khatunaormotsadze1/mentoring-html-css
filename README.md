# Mentoring-HTML-CSS

Name surName - Khatuna ormotsadze;
Achievements - after weeks of hard work and dedication, I finally achieved my dream of making a full responsive landing page ;
I'm proud because:
-- I have finished studying at a university;
-- I choose front-end development;

# Heading with Mentoring-HTML-CSS

## Sub-Heading with Name Surname

### Sub-Heading with Achievements

- **Education:** I graduated Sokhumi State University, Faculty of Natural Sciences, Mathematics, Technologies and Pharmacy.
- **Personal Projects:** My friends and I created a little project about university databases, where we created a database diagram.
- **Professional Experience:** I worked in a pharmacy company as a pharmacist.
- **Skills:** HTML,CSS.
- **Certifications:** Skillwill academy certificate.
- **Contributions:** My friend and I created a diagram for a university database.
- **Awards and Honors:** I don't have any awards or honors .
- **Publications:** No, I don't have any publications.




